// Currently supports only 32-bit base instructions
//package decoder;

import common_types::*;
import BUtils::*;
`include "common_params.bsv"

function DecodeOut fn_decoder(Bit#(32) inst);

    Bit#(5) opcode = inst[6:2];
    Bit#(5)rd = inst[11:7];
    Bit#(3)funct3 = inst[14:12];
    Bit#(5)rs1 = inst[19:15];
    Bit#(5)rs2 = inst[24:20];
    Bit#(7)funct7 = inst[31:25];

    Op1type rs1type = IntegerRF;
    Op2type rs2type = IntegerRF;

    // Identify the type of intruction first
    Bool stype = (opcode == 'b01000);
    Bool btype = (opcode == 'b11000);
    Bool utype = (opcode == 'b01101 || opcode == 'b00101);
    Bool jtype = (opcode == 'b11011);
    Bool csrtype = (opcode == `SYSTEM_INSTR_op) ;
    Bool itype = (opcode == 'b00100 || opcode == 'b00000 || opcode == 'b00011);

    // Getting immediate value. By default, instruction is I-type
    Bit#(1) bit0 = inst[20];
    if(stype)
        bit0 = inst[7];
    else if(btype || utype || jtype)
        bit0 = 0;

    Bit#(4) bit1_4 = inst[24:21];
    if(stype || btype)
        bit1_4 = inst[11:8];
    else if(utype)
        bit1_4 = 0;

    Bit#(6) bit5_10 = inst[30:25];
    if(utype)
        bit5_10 = 0;

    Bit#(1) bit11 = inst[31];
    if(btype)
        bit11 = inst[7];
    else if(utype)
        bit11 = 0;
    else if(jtype)
        bit11 = inst[20];

    Bit#(8) bit12_19 = duplicate(inst[31]);
    if(utype || jtype)
        bit12_19 = inst[19:12];

    Bit#(11) bit20_30 = duplicate(inst[31]);
    if(utype)
        bit20_30 = inst[30:20];

    Bit#(1) bit31 = inst[31];

    Bit#(32) immediate_value = {bit31, bit20_30, bit12_19, bit11, bit5_10, bit1_4, bit0};

//---------------------------------------------------------------------
    if((opcode == `JALR_op) || jtype || utype || itype || csrtype)
        rs2 = 0;
    if(jtype || utype || (csrtype && (funct3 == 0)))
        rs1 = 0;
    if(btype || stype)
        rd = 0;
//-----------------------------------------------------------------------

    if(opcode == `JAL_op || opcode == `JALR_op|| opcode == `AUIPC_op)
        rs1type = PC;
    if(opcode == `JALR_op || opcode == `JAL_op || opcode == `FENCE_op)
      rs2type = Constant4;
    else if(itype || utype || jtype)
      rs2type = Immediate;
//-----------------------------------------------------------------------

    // Identifying intruction type
    Instruction_type inst_type = ALU;
    case (opcode[4:3])
        2'b00: case (opcode[2:0])
                    3'b000: inst_type = MEMORY;   //Load
                    3'b011: inst_type = MEMORY;   //Fence, Fence.I
                    //3'b100: inst_type = ALU;  // OP-Imm
                    //3'b101: inst_type = ALU;  // AUIPC
                endcase
        2'b01: case (opcode[2:0])
                    3'b000: inst_type = MEMORY;  // Store
                    //3'b100: inst_type = ALU;     // OP-OP and MU
                    //3'b101: inst_type = ALU;     // LUI
                endcase
        2'b11: case (opcode[2:0])
                    3'b000: inst_type = BRANCH;   // conditional branches
                    3'b001: inst_type = JALR;
                    3'b011: inst_type = JAL;
                endcase
    endcase
//------------------------------------------------------------------------

    // Access Type
    Access_type mem_access = Load;
    if(opcode[0] == 1'b1)
        mem_access = Fence;
    else if(opcode[3] == 1'b1)
        mem_access = Store;
//------------------------------------------------------------------------

    Bit#(4) fn = 0;
        if(opcode == `BRANCH_op )begin
            if(funct3[2] == 0)
                fn={2'b0, 1,funct3[0]};
            else
                fn={1'b1, funct3};
        end
        else if(opcode == `IMM_ARITH_op )begin
            fn = case(funct3)
                3'b010 : 4'b1100;
                3'b011 : 4'b1110;
                3'b101 : if(funct7[5] == 1) 4'b1011; else 4'b0101;
                default:{1'b0, funct3};
            endcase;
        end
        else if(opcode == `ARITH_op )begin
            fn = case(funct3)
                3'b000 : if(funct7[5] == 1) 4'b1010; else 4'b0000;
                3'b010 : 'b1100;
                3'b011 : 'b1110;
                3'b101 : if (funct7[5] == 1) 4'b1011;else 4'b0101;
                default:{1'b0, funct3};
            endcase;
        end

    Bit#(7) temp_funct = {fn, funct3};

    let op_addr = OpAddr{rs1addr : rs1, rs2addr : rs2, rd : rd };
    let op_type = OpType{rs1type : rs1type, rs2type : rs2type };
    let instr_meta = InstrMeta{inst_type : inst_type, memaccess : mem_access, funct : temp_funct, immediate : immediate_value};
    return DecodeOut{op_addr : op_addr, op_type : op_type, meta : instr_meta};
endfunction: fn_decoder

module mk_decoder_tb(Empty);
    Bit#(32) inst = 'h00208093;
    DecodeOut decoded_inst = fn_decoder(inst);
    rule display;
        $display("rs1addr=%b, rs2addr=%b, rdaddr=%b, rs1type=%b, rs2type=%b, inst_type=%b, mem_access=%b, immediate=%b, funct=%b",
            decoded_inst.op_addr.rs1addr,
            decoded_inst.op_addr.rs2addr,
            decoded_inst.op_addr.rd,
            pack(decoded_inst.op_type.rs1type),
            pack(decoded_inst.op_type.rs2type),
            pack(decoded_inst.meta.inst_type),
            pack(decoded_inst.meta.memaccess),
            decoded_inst.meta.immediate,
            decoded_inst.meta.funct);
        $finish;
    endrule
endmodule: mk_decoder_tb

